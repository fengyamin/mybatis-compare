# Mybatis-Flex 和 Mybatis-Plus 性能对比

## 测试方法

使用 h2 数据库，在初始化的时候分别为 flex 和 plus 创建两个不同的数据库，
但是完全一样的数据结构、数据内容和数据量（每个库 2w 条数据）。

直接进行预热，之后通过打印时间戳进行对比，消耗的时间越少，性能越高（每次测试 5 轮）。

**测试步骤：**

- 1、clone 代码。
- 2、执行 `mvn clean package` 编译。
- 2、运行 `TestStarter` 的 `main` 方法。

## 查询单条数据

> Mybatis-Flex 的性能大概是 Mybatis-Plus 的 5 ~ 10+ 倍。

以下是日志，需要注意的是：每台机器的每次测试的数值肯定有所不同，但是应该会维持一个大概的比例。

```
---------------
---------------
>>>>>>>testFlex-SelectOne:74
>>>>>>>testPlus-SelectOneWithLambda:633
>>>>>>>testPlus-SelectOne:535
---------------
>>>>>>>testFlex-SelectOne:67
>>>>>>>testPlus-SelectOneWithLambda:724
>>>>>>>testPlus-SelectOne:455
---------------
>>>>>>>testFlex-SelectOne:49
>>>>>>>testPlus-SelectOneWithLambda:472
>>>>>>>testPlus-SelectOne:1158
---------------
>>>>>>>testFlex-SelectOne:36
>>>>>>>testPlus-SelectOneWithLambda:352
>>>>>>>testPlus-SelectOne:311
---------------
>>>>>>>testFlex-SelectOne:35
>>>>>>>testPlus-SelectOneWithLambda:363
>>>>>>>testPlus-SelectOne:360
---------------
```

## 查询 List 数据，限制 10 条

> Mybatis-Flex 的性能大概是 Mybatis-Plus 的 5~10 倍左右

```
---------------
---------------
>>>>>>>testFlex-SelectTop10:41
>>>>>>>testPlus-SelectTop10WithLambda:291
>>>>>>>testPlus-SelectTop10:299
---------------
>>>>>>>testFlex-SelectTop10:35
>>>>>>>testPlus-SelectTop10WithLambda:275
>>>>>>>testPlus-SelectTop10:262
---------------
>>>>>>>testFlex-SelectTop10:36
>>>>>>>testPlus-SelectTop10WithLambda:293
>>>>>>>testPlus-SelectTop10:284
---------------
>>>>>>>testFlex-SelectTop10:35
>>>>>>>testPlus-SelectTop10WithLambda:273
>>>>>>>testPlus-SelectTop10:263
---------------
>>>>>>>testFlex-SelectTop10:34
>>>>>>>testPlus-SelectTop10WithLambda:272
>>>>>>>testPlus-SelectTop10:269
---------------
```

## 分页查询

> Mybatis-Flex 的性能大概是 Mybatis-Plus 的 5~10 倍左右

```
>>>>>>>testFlex-Paginate:36
>>>>>>>testPlus-Paginate:301
---------------
>>>>>>>testFlex-Paginate:38
>>>>>>>testPlus-Paginate:287
---------------
>>>>>>>testFlex-Paginate:33
>>>>>>>testPlus-Paginate:266
---------------
>>>>>>>testFlex-Paginate:32
>>>>>>>testPlus-Paginate:294
---------------
>>>>>>>testFlex-Paginate:33
>>>>>>>testPlus-Paginate:333
---------------
```

## 数据更新

> Mybatis-Flex 的性能大概是 Mybatis-Plus 的 5~10+ 倍左右。

```
---------------
>>>>>>>testFlex-Update:33
>>>>>>>testPlus-Update:287
---------------
>>>>>>>testFlex-Update:30
>>>>>>>testPlus-Update:297
---------------
>>>>>>>testFlex-Update:24
>>>>>>>testPlus-Update:241
---------------
>>>>>>>testFlex-Update:22
>>>>>>>testPlus-Update:251
---------------
>>>>>>>testFlex-Update:25
>>>>>>>testPlus-Update:271
---------------

```

